﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Frases_V2
{
    public partial class Contenido_Frases : PhoneApplicationPage
    {
        public Contenido_Frases()
        {
            InitializeComponent();
        }

        private void botonMB_Click(Object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pag_MarioBenedetti.xaml", UriKind.Relative));
        }

        private void botonGGM_Click(Object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pag_GGarciaMarquez.xaml", UriKind.Relative));
        }

        private void botonPN_Click(Object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Pag_PabloNeruda.xaml", UriKind.Relative));
        }

        private void botonWS_Click(Object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/WilliamShakespeare.xaml", UriKind.Relative));
        }

        private void botonOW_Click(Object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/OscarWilde.xaml", UriKind.Relative));
        }
    }
}